<?php

namespace Elements\Economic\Test\TestCase;

use Elements\Economic\Economic;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Subscriber\Mock;
use GuzzleHttp\Message\Response;


/**
 * Class EconomicTest
 *
 * @package Elements\Economic\Test\TestCase
 */
class EconomicTest extends \PHPUnit_Framework_TestCase {

	/**
	 *
	 */
	public function setUp() {
		parent::setUp();
	}

	/**
	 *
	 */
	public function testInit() {

		try {
			$client = new Economic();
			$this->fail('No exception raised');
		} catch (\RuntimeException $e) {
			$this->assertContains('Please provide appId and accessToken', $e->getMessage());
		}

		$client = new Economic('appId', 'AccessKey');
		$this->assertInstanceOf('Elements\Economic\Economic', $client);
		$this->assertObjectHasAttribute('client', $client);
		$this->assertInstanceOf('GuzzleHttp\Client', $client->client);

	}

	public function testSendRequest() {

		$client = new Economic('appId', 'AccessKey');
		$mock   = new Mock([
			new Response(200, ['X-Foo' => 'Bar'], Stream::factory('{"some":"value"}')),
			new Response(403, ['X-Foo' => 'Bar'], Stream::factory('{"some":"value"}'))

		]);
		$client->client->getEmitter()->attach($mock);

		$out = $client->sendRequest('/customers?pagesize=1000', 'get');
		$this->assertEquals(['some' => 'value'], $out);

		try {
			$out = $client->sendRequest('/customers?pagesize=1000', 'get');
			$this->fail('No exception raised');
		} catch (\RuntimeException $e) {
			$this->assertContains('Access Denied', $e->getMessage());
		}
	}

}
