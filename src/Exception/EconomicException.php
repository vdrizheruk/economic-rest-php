<?php

namespace Elements\Economic\Exception;

use Cake\Core\Exception\Exception;

class EconomicException extends Exception {

	public function __construct($message = null) {
		if (empty($message)) {
			$message = 'Unauthorized';
		}
		parent::__construct($message);
	}

}


