<?php

namespace Elements\Economic;

use Elements\Economic\Exception\EconomicException;
use GuzzleHttp;

/**
 * Class Economic
 *
 * @package Elements\Economic
 */
class Economic {

	/**
	 * @var string
	 */
	private $appId;

	/**
	 * @var string
	 */
	private $accessId;

	/**
	 * @var GuzzleHttp\Client
	 */
	public $client;


	/**
	 * @param string $appId
	 * @param string $accessId
	 */
	public function __construct($appId = null, $accessId = null) {
		if (!$appId || !$accessId) {
			throw new EconomicException('Please provide appId and accessToken');
		}
		$this->appId    = $appId;
		$this->accessId = $accessId;

		/**
		 * Client
		 */
		$this->client = new GuzzleHttp\Client([
			'base_url' => 'https://restapi.e-conomic.com',
			'defaults' => [
				'headers' => [
					'accept'   => 'application/json',
					'appId'    => $this->appId,
					'accessId' => $this->accessId
				]
			]
		]);
	}

	/**
	 * @param $endpoint
	 * @param string $method
	 * @param array $data
	 * @param array $options
	 */
	public function sendRequest($endpoint, $method = 'get', $options = []) {

		try {
			/* @var \GuzzleHttp\Message\ResponseInterface $response */
			$response = $this->client->get($endpoint, $options);

			return $response->json();

		} catch (GuzzleHttp\Exception\ClientException $e) {

			throw new EconomicException('Access Denied');

		}
	}

}